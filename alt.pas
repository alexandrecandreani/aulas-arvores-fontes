program alt;
uses crt;
 

// tipo da chave
type tipoItem = record
                  chave: integer;
                  alt:integer;
                end;
                
                
apontador = ^no;

   no = record
            item: tipoItem;
             esq: apontador;
             dir: apontador;
          end;
// raiz
var raiz: apontador;
input:integer; 
item:tipoItem=(chave:0;alt:0)  ;
c:char;
ehEntradaManual:boolean=false;


procedure visitaRaiz(p:apontador);
begin

	
	writeln('(c:',p^.item.chave, ';a:',p^.item.alt ,'), ');
end;
//insere
procedure insere(x: tipoItem; 
  var p: apontador);
begin
  if p = nil
     then begin
            new(p);
            p^.item := x;
            p^.esq := nil;
            p^.dir := nil;
          end
  else if (x.chave < p^.item.chave)
          then insere(x, p^.esq)
       else insere(x, p^.dir);
end;

//percurso pos-ordem
procedure pos_ordem(p: apontador);
begin
  if (p <> nil)
     then begin
            pos_ordem(p^.esq);
            pos_ordem(p^.dir);
            visitaRaiz(p);
          end;
end;

//percurso pos-ordem
procedure visitaRaiz_alt(p:apontador;alt:integer);
begin
	p^.item.alt:=alt;
	writeln('(c:',p^.item.chave, ';a:',p^.item.alt ,'), ');
	
end;

function pos_ordem_alt(p: apontador):integer;
var 
alt,alt2:integer;

begin 
	alt:=0;
	if (p <> nil)then 
	begin   
		alt:=pos_ordem_alt(p^.esq);
		alt2:=pos_ordem_alt(p^.dir);
	
		if alt2>alt then
			alt:=alt2;
		alt:=alt+1;
			
		visitaRaiz_alt(p,alt);
	end ;
		pos_ordem_alt:= alt; 
end;

begin

//entrada manual de dados
if (ehEntradaManual) then
begin
	writeln('Exercício 1'); 
	repeat
		writeln('entre com um valor Inteiro');
		readln(input);
		item.chave := input;
		insere(item,raiz);
		writeln('mais?(n,s)');
		read(c);
		readln;
	until (c='n');
end
else
//entrada estática : 33,15,41,38,47,34,43,49
begin
item.chave := 33;
insere(item,raiz);
item.chave := 15;
insere(item,raiz);
item.chave := 41;
insere(item,raiz);
item.chave := 38;
insere(item,raiz);
item.chave := 47;
insere(item,raiz);
item.chave := 34;
insere(item,raiz);
item.chave := 43;
insere(item,raiz);
item.chave := 49;
insere(item,raiz);
end;
//fim entrada estática

writeln('pos ordem_alt');
pos_ordem_alt(raiz);


writeln('pos ordem');
pos_ordem(raiz);

	 
end.

