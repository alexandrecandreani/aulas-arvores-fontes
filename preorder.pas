program preorder;
uses crt;
 

// tipo da chave
type tipoItem = record
                  chave: integer; 
                end;
                
                
apontador = ^no;

   no = record
            item: tipoItem;
             esq: apontador;
             dir: apontador;
          end;
// raiz
var raiz: apontador;
input:integer; 
item:tipoItem=(chave:0);
c:char;
ehEntradaManual:boolean=false;


procedure visitaRaiz(p:apontador);
begin
	writeln(p^.item.chave, ', ');
end;
//insere
procedure insere(x: tipoItem; 
  var p: apontador);
begin
  if p = nil
     then begin
            new(p);
            p^.item := x;
            p^.esq := nil;
            p^.dir := nil;
          end
  else if (x.chave < p^.item.chave)
          then insere(x, p^.esq)
       else insere(x, p^.dir);
end;

//percurso pre-ordem
procedure pre_ordem(p: apontador);
begin
  if (p <> nil)
     then begin
		visitaRaiz(p);	            
		pre_ordem(p^.esq);
            pre_ordem(p^.dir);            
          end;
end;


begin

//entrada manual de dados
if (ehEntradaManual) then
begin
	writeln('Exercício 1'); 
	repeat
		writeln('entre com um valor Inteiro');
		readln(input);
		item.chave := input;
		insere(item,raiz);
		writeln('mais?(n,s)');
		read(c);
		readln;
	until (c='n');
end
else
//entrada estática : 33,15,41,38,47,34,43,49
begin
item.chave := 33;
insere(item,raiz);
item.chave := 15;
insere(item,raiz);
item.chave := 41;
insere(item,raiz);
item.chave := 38;
insere(item,raiz);
item.chave := 47;
insere(item,raiz);
item.chave := 34;
insere(item,raiz);
item.chave := 43;
insere(item,raiz);
item.chave := 49;
insere(item,raiz);
end;
//fim entrada estática
 
writeln('pre ordem');
pre_ordem(raiz);

	 
end.

